package com.wemirr.platform.authority.service;

import com.wemirr.framework.boot.service.SuperService;
import com.wemirr.framework.database.mybatis.auth.DataScope;
import com.wemirr.platform.authority.domain.dto.RoleDTO;
import com.wemirr.platform.authority.domain.entity.Role;

import java.util.List;

/**
 * <p>
 * 业务接口
 * 角色
 * </p>
 *
 * @author Levin
 * @since 2019-07-03
 */
public interface RoleService extends SuperService<Role> {


    List<Role> list(DataScope scope);


    /**
     * 根据ID删除
     *
     * @param ids
     * @return
     */
    boolean removeByIdWithCache(List<Long> ids);

    /**
     * 保存角色
     *
     * @param data
     * @param userId 用户id
     */
    void saveRole(Long userId, RoleDTO data);

    /**
     * 修改
     *
     * @param role
     * @param userId
     */
    void updateRole(Long roleId, Long userId, RoleDTO role);


    void saveUserRole(Long roleId, List<Long> userIdList);
}
