package com.wemirr.platform.authority.mapper;

import com.wemirr.framework.boot.SuperMapper;
import com.wemirr.framework.database.mybatis.auth.DataScope;
import com.wemirr.platform.authority.domain.entity.Role;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Levin
 */
@Repository
public interface RoleMapper extends SuperMapper<Role> {


    List<Role> findRoleByUserId(Long userId);

    List<Role> list(DataScope scope);
}
