package com.wemirr.platform.authority.mapper;

import com.wemirr.framework.boot.SuperMapper;
import com.wemirr.platform.authority.domain.entity.log.OptLog;
import org.springframework.stereotype.Repository;

/**
 * @author Levin
 */
@Repository
public interface OptLogMapper extends SuperMapper<OptLog> {
}
